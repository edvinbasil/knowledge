# Introduction

Hi, Welcome to my personal wiki. This is meant to be a sink for all the things
that I want to keep notes of.
This was heavily inspired by the motto of **Document Everything** by
[@yoshuawuyts](https://github.com/yoshuawuyts), from his knowledge repository.
And thus, might be apparent from the similar title

This book is rendered using [mdbook](https://github.com/rust-lang/mdBook),
which is a utility to create books from markdown files, and is heavily used in
the [rust](https://www.rust-lang.org/) ecosystem.
