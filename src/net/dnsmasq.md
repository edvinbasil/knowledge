# dnsmasq

Static routes may be issued to clients using the dns option 121
(classless-static-route) [RFC3442](https://datatracker.ietf.org/doc/html/rfc3442). 

eg: This configures the route "10.10.10.0/24 via 192.168.1.100"

> Note: there is also the older option  33 (static-route) which allows
> specifying classful addresses. It doesn't allow classless CIDR notation.

```
dhcp-option=option:classless-static-route,0.0.0.0/0,192.168.1.1,10.10.10.0/24,192.168.1.100
```

Stating the 0.0.0.0/0 route is also requires as clients ignore the default route
if a classless route is issued.

From [RFC3442 Sec: DHCP Client Behavior](rfc3442)
```
If the DHCP server returns both a Classless Static Routes option and
a Router option, the DHCP client MUST ignore the Router option.
```
