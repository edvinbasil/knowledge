# Routing

## Dynamic Routing

Protocols can be static or dynamic

Dynamic can be of different types

- IGP : Interior Gateway protocol 
    - Distance Vector (aka Routing by rumour since routers only get data from
      neighbours, not directly from other routers)
        - RIP (uses hop count)
        - EIGRP (metric based on lowest bandwidth and route delays)
    - Link State
        - OSPF (cost of each link in the route)
        - IS-IS (cost of each link but costs not calculated automatically. all
          costs are 10 by default)
- EGP (Exterior Gateway protocol)
    - Path Vector
        - BGP

Dynamic routing protocols use different metrics to choose the best route

Metrics cannot be compared between protocols

**ECMP**: Equal Cost Multi Path : When metric is same for two routes, both the
routes are added and traffic is load balanced between them

**AD**: Administrative Distance : Signifies how trustworthy a route is : Lower
AD routes will have higher priority. Routes learned through protocols with lower
AD will be preferred regardless of the metrics since those metrics cant be
compared directly. Note that AD values can be changed manually. Given are some of the ADs:

| Type/Protocol      | AD  |
| -------------      | --  |
| Directly connected | 0   |
| Static             | 1   |
| eBGP               | 20  |
| EIGRP              | 90  |
| IGRP               | 100 |
| OSPF               | 110 |
| IS-IS              | 115 |
| RIP                | 120 |
| EIGRP(external)    | 170 |
| iBGP(internal)     | 200 |
| Unusable route     | 255 |

