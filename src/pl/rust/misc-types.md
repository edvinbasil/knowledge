# VecDequeue

`std::collections::VecDequeue`

A vector implementation of a queue. Tracks start and end positions and acts as a
ring buffer to give `push_back` and `pop_front` methods.
