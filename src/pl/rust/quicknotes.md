# QuickNotes

## [The Unsafe Chronicles: Exhibit A: Aliasing Boxes](https://www.youtube.com/watch?v=EY7Wi9fV5bk)

- [`std::mem::transmute`](https://doc.rust-lang.org/std/mem/fn.transmute.html) : dont use unit absolutely necessary. you can do reaaaly
unsound things with this
- [`std::mem::ManuallyDrop`](https://doc.rust-lang.org/std/mem/struct.ManuallyDrop.html) : Prevents calling `T`'s [destructor](https://doc.rust-lang.org/reference/destructors.html). Allows us to
provide our own drop rules
- [`std::mem::MaybeUninit`](https://doc.rust-lang.org/std/mem/union.MaybeUninit.html) : Wrapper that says that the content might not be
valid yet. Usually the compiler has some rules that are *always* upheld and thus
make optimizations for it. MaybeUninit signals the compiler to assume nothing
about the type and thus prevent these optimizations.
- [`repr(transparent)`](https://doc.rust-lang.org/reference/type-layout.html#the-transparent-representation)
- [`std::marker::PhantomData`](https://doc.rust-lang.org/std/marker/struct.PhantomData.html) : Used to carry around info at compile time. Is 0 sized. Also check [the nomicon](https://doc.rust-lang.org/nomicon/phantom-data.html)
