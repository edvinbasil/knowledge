# Macros

Macros are basically code that generates other code.
There are mainly 2 kinds of macros in rust:

- Declarative macros
- Procedural macros/Proc Macros

Jonhoo did a nice beginner friendly video on declarative macros on his channel
[here](https://www.youtube.com/watch?v=q6paRBbLgNw)

## Interesting macro patterns

- Seen in [smol](https://crates.io/crates/smol)

```rust

#[macro_export]
macro_rules! my_macro {
    ($($expr:tt)*) => {
        $crate::MyStruct::myfunc(async move { $($expr)* }).await
    };
}

```
