# Rust

[Homepage](https://www.rust-lang.org/) | [Book](https://doc.rust-lang.org/stable/book/)

Rust has a unique ownership model that allows it to guranee memory safety and
run without a garbage collector.  
From all the programming languages I've come across, rust defenitely has its
unique feel to it and requires a different way of thinking.  
It has also made me explore design patterns other than
[OOP](https://en.wikipedia.org/wiki/Object-oriented_programming) (which is taught
extensively in schools and colleges)

Highly recommend it if you are interested in high performance or low level
languages. Also check it out if you are into high level programming because
rust can be quite friendly once you get used to it.

## Links

- [Rust API Guidelines](https://rust-lang.github.io/api-guidelines/)
    A nice set of tips on how to design your APIs in rust
- [A reddit discussion on what Cell and RefCell
    does](https://www.reddit.com/r/rust/comments/755a5x/i_have_finally_understood_what_cell_and_refcell/)

## Cool stuff to know about

- [Rust journey to async/await](https://www.youtube.com/watch?v=lJ3NC-R3gSI):
    A cool talk by Steve Klabnik, where he nicely presented the problems faced
    and their solutions to designing rust's async/await.

## Short notes

### as_ref

`as_ref()` has the signature `&Option<T> -> Option<&T>`

it turns a reference to an Option into an Option that holds a reference

if we siply try to do `opt.unwrap();` with a reference to an option,
 it will complain that it cannot mov out of borrowed content.

We only have a reference to the `Option`. unwrapping tries to move the value
inside to option out of it. which is not possible with shared references.

### Error handling in libraries and binaries

When writing a library, Its common to implement the `From<MyCustomErr>` trait
for the standard library `Error` type. This allows us to use `std::Result` along
with a custom error type that implements `std::error::Error`. This enables the
users of our library to use our error type, the same way they use the standard
Error type.  
But writing all the implementations can get boring and often quite repetitive.
[thiserror](https://crates.io/crates/thiserror) is a crate that provides
a convinient macro to derive the `std::error::Error` trait for our custom error
type.

When writin a binary, using [anyhow](https://crates.io/crates/anyhow) gives us
some nice convinience traits that is implemented for return types like the
`context()` method: which allows us to add custom error messages when our
application returns an error. It also provides an `Error` type that, when used
in the main function, automatically formats the errors nicely to stdout
