# Packages

A list of useful rust packages

- https://github.com/nikomatsakis/rayon - Parallelism library for iterable tasks
- https://github.com/Amanieu/hashbrown - Faster HashMap and HashSet, drop-in replacement
- https://github.com/dtolnay/thiserror - Convinient macro to derive Error for custom error types
