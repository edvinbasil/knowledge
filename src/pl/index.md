# Programming Languages

Well, what did you expect? I am a Computer Science and Engineering student.
Having said that, here are a few cool resources I found:

- [Learn Anything: Programming
    languages](https://learn-anything.xyz/programming/programming-languages)
- [Githut](https://githut.info/) A small place to discover languages in GitHub
