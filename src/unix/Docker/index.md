# Docker

## Writing dockerfiles

- try to use a smaller base image for prod. usually alpine is a a good choice
  unless you have distro specific features that you need
- Specify the version tags for the image explicitly.
  eg `nginx:1.4-alipne3.1`
- use a non-root user for most tasks especially the execution process
- use multi stage builds to reduce the size of the final image. eg:

```Dockerfile
# ----- Initial stage with build dependencies ------

FROM node:12.10.4-alpine3.2 AS builder

# Specify ENV vars here
# Specify WORKDIR here if needed
# do the required COPY here
# RUN pre-exec scripts here

# ... do build steps here
# specifying a seperate user here is not really necessary for multistage
# containers since they dont really affect the final runtime

# --------------- NEXT STAGE-----------------

FROM alipne:3.2 as runtime

USER 1000 

# Specify all the above (workdir, envs, etc here)
WORKDIR /app

# copy over any non-dependent files/static assets here
COPY static/ static/
# copy files over from seperate stage
COPY --from=builder /app/build/mybin /app/mybin

# expose the required port here
EXPOSE 8888

# Specify the final runtime command
CMD [ "/app/mybin" ]

```
