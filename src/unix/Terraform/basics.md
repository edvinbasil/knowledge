# Basic Usage

- Terraform uses all files in the current dir that end in `tf`
- `terraform init` initialises the current project and fetches the providers
- `terraform plan` shows the changes that are to be done in the next apply stage
- `terraform apply` applis the configuration to the infrastructure
- the state is stored in a `.tfstate` file. This keeps tracks of the resources
  it manages
- `terraform show` displays the details of the resources it manages
- `terraform destory` tears down all resources managed by the configuration
- `terraform fmt` formats the configuration for consistency
- `terraform validate` validates and finds any syntactical mistakes in the
  config
- `--auto-approve` to skip the confirmation while applying
- `terraform state list` to show the resources we manage
- `terraform state show <resource_name>` to show details and other attributes of
  a resource
- `terraform output` shows the output from the state file
- `terraform refresh` refreshes the state file from the actual resources
- adding a `-target=<name>` to apply/destroy allows us to create/destroy
  individual resources
