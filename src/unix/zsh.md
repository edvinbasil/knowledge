# Zsh

[Manual](http://zsh.sourceforge.net/)

`$ZDOTDIR` : Root directory for user level zsh configuration. Usually set to ~  
`$ZDOTDIR/.zshenv` : For setting env variables. This is always read  
`$ZDOTDIR/.zprofile` : Read on **login** shells. Useful to startup window
managers  
`$ZDOTDIR/.zshrc` : Read on **interactive** shells. This is always read when
starting a shell unless -f added  
`$ZDOTDIR/.zlogin` : read at the end of **login** process. Useful for starting cli
daemons/utilities  
`$ZDOTDIR/.zlogout` : read when **login** shell exits. Can be used to
unset variables  

Each of these files can have a system-wide counterpart at `/etc/zsh`

## Some cool tips

- Add `typeset -U PATH path` to your `.zshenv` or `.zshrc` to remove duplicates
    from the `PATH` variable [Note: zsh auto syncs path with PATH. Hence using
    -U on both variables]
- [ZSH Custom widgets and shortcuts with zle and bindkey](https://sgeb.io/posts/2014/04/zsh-zle-custom-widgets/)
