# Kubernetes

[Main Documentation](https://kubernetes.io/docs/home/)

Kubernetes is a container orchestration engine

## Basic terminology

- __node:__ A single VM or physical machine running kubernetes. There can be 
  master nodes and worker nodes
- __pods:__ smallest deployable unit in a cluster. can consist of multiple containers
- __replica set:__ gurantees a number of pods are up at any given time
- __deployments:__ defines a desired state of pods. uses replication controllers
- __services:__ rules to expose a deployment. provides virtual network addresses
- __volumes:__ provides persistency and file sharing between pods
- __namespace:__ a way to split a cluster into seperate virtual clusters

## Useful commands

- `get x -o wide` [ get more info , eg for pods show its ip]
- `create x -o yaml --dry-run > file.yml` , edit and apply back
- `helm inspect values stable/nnextcloud > values.yml` gets the values file
- `helm install my_name -f values.yml` to specify the values file
- `service: type NodePort` to use in ingress controller
- `persistentvolumeclaims` to create claims, which can be fullfilled by the provider
- `port-forward <resource> locport:remoteport`
- `kubectl edit <resource>` : easy editing deployments
- `kubectl exec -it bash` : interactive terminal
- `kubectl explain <resource> [ --recursive ]` : short documentation on resource
- `kubectl get resourcequota` : get limits for each resource


## Kubernetes Resources

- [Examples](https://github.com/kubernetes/examples) for writing yamls

# Related

- [Docker](../Docker/index.md)
