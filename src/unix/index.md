# Unix

Useful notes on unix / linux commands

# Links

Apparently everything has a handbook now

- [Debian Handbook](https://debian-handbook.info/browse/stable/)
- [FreeBSD Handbook](https://docs.freebsd.org/en/books/handbook/)
- [OpenBSD Handbook](https://www.openbsdhandbook.com/)
- [SLES Docs](https://documentation.suse.com/sles/15-SP2/)
