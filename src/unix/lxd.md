# LXD

## Useful links

- https://stgraber.org/category/lxd/

## Map same uid/gid from host to container

```sh
# Mapping controlled by the shadow set of utilities
# Allow lxd to use my uid and gid
printf "lxd:$(id -u):1\nroot:$(id -u):1\n" | sudo tee -a /etc/subuid
printf "lxd:$(id -g):1\nroot:$(id -g):1\n" | sudo tee -a /etc/subgid

sudo systemctl restart lxd


# map my uid to uid:1000 in the container and my gid to gid:1000 in the container
printf "uid $(id -u) 1000\ngid $(id -g) 1000" | lxc config set mycontainer raw.idmap -
lxc restart mycontainer
```

