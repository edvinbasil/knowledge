# SELinux

> Mostly notes from [Security-Enhanced Linux for mere
> mortals](https://www.youtube.com/watch?v=\_WOKRaM-HI4) by Thomas Cameron |
[Redhat: SELinux User's and Administrator's Guide](https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/7/pdf/selinux_users_and_administrators_guide/Red_Hat_Enterprise_Linux-7-SELinux_Users_and_Administrators_Guide-en-US.pdf) |
[The arch wiki article](https://wiki.archlinux.org/title/SELinux)

SELinux policy:

The policy can be found at `/etc/selinux/config`,
which is also symlinked to `/etc/sysconfig/selinux`

```
SELINUX= can take one of these three values:
    enforcing - SELinux security policy is enforced.
    permissive - SELinux prints warnings instead of enforcing.
    disabled - No SELinux policy is loaded.

SELINUXTYPE= can take one of three values:
    targeted - Targeted processes are protected,
    minimum - Modification of targeted policy. Only selected processes are protected. 
    mls - Multi Level Security protection.
```

It can also be seen via `sestatus`

The current mode can be viewed using `getenforce`


## Labeling

Files, Ports etc are labelled with an SELinux context.
for files and directories, extended attrs are used to store these labels while
kernel manages the labels for processes and such.

They are of the format

`user:rele:type:level(optional)`
  
  The `user` above is the selinux user and is different from the normal linux
  user

just the `type` can be used for basic usecases

selinux adds the `-Z` option to several commands  to view the labels associated
with the associated resources. A few of them are:

- `ls -Z`
- `id -Z`
- `ps -Z`
- `netstat -Z`




## Type Enforcement

The types in the labels are usually related for resources that belong together.

Eg. for apache, the following types are observed:

- `httpd_config_t` for its config directory
- `httpd_log_t` for its log directory
- `httpd_sys_content_t` for the webroot
- `httpd_initrc_exec_t` for the init script
- `httpd_t` for the process

Type enforcement is the part of the policy that says, for instance, “a process
running with the label httpd_t can have read access to a file labeled
httpd_config_t"

## Working with labels

`chcon` or `restorecon` may be used to change the context of a file

When a file is created, it inherits the context of its parent (wiht a few
exceptions)

The login process sets the default context, which is unconfined for the
`targeted` policy

- **File transitions** (defined by policy) may be set such that if an application
foo_t creates a file in a directory labeled bar_t, it ges the baz_t label.

- `getsebool -a` shows all booleans available
- use `setsebool <boolean_name> <0|1>` to set the boolean temporarily
    - use the `-P` argument with it to make the change persistent

> Tip: Install ` setroubleshoot` and `setroubleshoot-server` on machines  used
> to develop policy modules. Reboot or restart auditd afterwards. This will
> make the logs in `/var/log/messages` more human-readable


The SELinux logs tells you what to do in most cases

A good place to check for currently actiove booleans is the `booleans.local`
file under `/etc/selinux/targeted/modules/active/`. 
Note: editing this file will not change anything.

Modifying policies using `setsebool` or other commands regenerates the
`/etc/selinux/targeted` directory. So sont bother changing anything in there.

use `man chcon` so see how to change the labels.

Usually to change just the type, use `chcon -t my_type_t /path/to/file`

- Or even just use `chcon --reference /known/good/file /path/to/target/file` to
- use the refernce file's security context
- Or maybe you just need to restore the context to its defaults. In this case,
  use `restorecon`. eg: `restorecon -vR /path/to/file`
    - `-v` is for verbose
    - `-R` is recursive
    - read man btw `restorecon(8)`
- `restorecon` uses information from the `/etc/selinux/targeted/contexts/files/file_contexts(*)`
   files to determine what a file or directory's context should be
- to add a new default context to be used, we use 
  `semanage fcontext -a -t my_type_t "/foo(/.*)?"`
    - `fcontext` : for file context `man semanage-fcontext(8)`
    - `-a` to add
    - `-t` for type
- or simply `semanage fcontext -a -e /known/good/file/ /foo/"`
    - `-e` for equal
- dont forget to use `restorecon` to apply the changes from the default context
  that was just set

- When working on creating a polllicy, a good way is to use a `permissive` mode
  for SELinux to simply log the messages. Then after going through the usage of
  the application, use something like `grep httpd /var/log/audit/audit.log
  | audit2allow -M mymodulelocal` to create a new policy based on the audit.
  Then use `semodule -i mymodulelocal.pp` to install (`-i`) the module
- `audit2why` can be used in a similar fashion to `audit2allow` to get
  a human-readable descriptions of the audits

## Enabling SELinux on a new system

Setting selinux to enforcing usually doesnt end so well

So first set it to permissive, touch a file called `.autorelabel` to the root of
the filesystem and reboot. This will relabel the whole filesystem to work with
selinux

After its done relabelling, set SELinux to enforcing
