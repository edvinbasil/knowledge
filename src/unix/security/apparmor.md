# AppArmor

> [Official Website](https://apparmor.net/) |
[Docs](https://gitlab.com/apparmor/apparmor/-/wikis/Documentation) |
[Wiki](https://gitlab.com/apparmor/apparmor/-/wikis/home)

Install the `apparmor-utils` package as well

Some common commands are:

- `aa-enabled`: Is apparmor enabled or not
- `aa-status`: Display the status of profile enforcement
- `aa-genprof`: Create an apparmor profile
- `aa-logprof`: Update and existing profile
- `aa-complain`: Set a profile in complain mode
- `aa-enforce`: Set a profile in enforce mode
- `apparmor_parser <-r|-a> /path/to/profile`to reload or add a profile

A profile in apparmor can run in various modes:

- `complain`: Apparmor allows a process to perform _almost_ all tasks but will log
them as events in the audit log. It will still enforce any explicit deny rules
in a profile.
- `unconfined`: Allows a program to perform any task and will not log it
- `enforced`: Apparmor enforces the rules specified in the profile

Common apparmor permissions

- `r`: read
- `w`: write
- `k`: lock
- `ux`: unconstrained execute
- `Ux`: unconstrained execute -scrub
- `px`: discrete profile execute
- `Px`: discrete profile execute -scrub
- `ix`: inherit execute
- `cx`: local seecurity profile
- `l`: link
- `a`: append
- `m`: memory map

Apparmor profiles are stored in `/etc/apparmor.d` by default

It also adds the `-Z` option to commands like `ps` to show what profile is being
used in different applications and in what mode
