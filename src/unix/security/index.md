# Security

LSM: Linux Kernel Security Modules.

Inserts hooks at every point in the kernel where a user can conduct a system
call to import kernel objects.

DAC: Discretionary Access Control : Default linux permissions
MAC: Mandatory Access Control

## AppArmor

Is an LSM that allows the user to restrict programs with the use of a profile
specifiv to that application. These profiles are designed to delegate
capabilities that include sockets, network access, file access and more.

Provides MAC on top of DAC

Enabling apparmor
```cfg
#: /boot/syslinux/syslinux.cfg 

APPEND apparmor=1 security=apparmor
```

Comes with sane defaults and have profiles for certain applications

### Useful links

- https://wiki.archlinux.org/title/AppArmor
- [The Comprehensive Guide To AppArmor](https://archive.is/5A8Ka)

## SELinux

Defines the access and transition rights of every user, application, process and
file on the system.

**Functioning**:

- A process will perform an action request. (eg:to read a file)
- THe request is pushed to the SELinux security server
- The security server checks an Access Vector Cache (AVC) that stores subject and
  object permissions.
- If request not found therem it will consult the SELinux Policy Database.
- If the policy is found, It makes a decision of Yes or No for the access
  request.
- If yes, the object requested is returned. Else an AVC denied message is
  generated and the object is not returned

### Useful links

- [Redhat: SELinux User's and Administrator's Guide](https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/7/pdf/selinux_users_and_administrators_guide/Red_Hat_Enterprise_Linux-7-SELinux_Users_and_Administrators_Guide-en-US.pdf)
- https://wiki.archlinux.org/title/SELinux

## Firejail

Uses linux namespaces  in conjunction with seccomp-bpf to restrict the running
enviornment of the application. 

It comes with security profiles for a lot of applications in /etc/firejail.
