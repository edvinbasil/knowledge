# Pandoc

## Filters
- `pandoc-citeproc`: Manage citations
- `pandoc-crossref`: Manage references and numbering

## Latex

Generate template using 

```
pandoc -D latex
```

## Makefile Example

```makefile
FILES = NOTE.md \
		meta/common.yaml

OUTPUT = build

FLAGS = -s \
		-F pandoc-crossref\
		-F pandoc-citeproc\
		--toc \
		--bibliography bib.bib

all: pdf pre

pdf:
	pandoc -o $(OUTPUT)/output.pdf $(FLAGS) $(FILES) meta/pdf.yaml
pre:
	pandoc -o $(OUTPUT)/presentation.pdf -t beamer $(FILES) meta/presentation.yaml

clean:
	rm build/*
```

## Metadata files

common.yaml
```yaml

---
title: Hello World
author: Edvin basil Samuval
---

```

pdf.yaml
```yaml

---
keywords:
  - one
  - two
  - three
numbersections: yes
lang: en
babel-lang: english
autoEqnLabels: true
geometry:
- top=30mm
- left=25mm
- right=25mm
- bottom=30mm
abstract: |
    Eyy
    Wassap

---
```

presentation.yaml
```yaml

---
theme: Berkeley
colortheme: seahorse
---

```
