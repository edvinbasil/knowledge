# IRC - Internet Relay Chat

IRC is a low-bandwidth method of communication. IRC is very similar to text
messaging, but designed around communicating with large groups of users instead
of one on one.
People familiar with discord can consider it to be the predecessr of discord and
many other similar messaging applications.

IRC has different networks of servers, to which a client may connect to. The
different networks hosts different channels in them.
Channel names usually start with a `#` sign

Each user on irc is identified by a `nick` or a `nickname`. A user may have
multiple nicknames associated to him, to prevent conflicts on different
channels.

## Weechat

Weechat is an irc client that is extremely customisable. And since its termial
app, it can be put in a tmux session, even on servers.
[Irssi](https://irssi.org/) is also a good alternative but I liked weechat a bit
better

Now, weechat does support mouse mode, and its nice to have enabled. but in some
cases you just want to use the keyboard.
But keyboard shortcuts are easy to forget if you take a break from using them.

## Useful Links

- [The Weechat user
    manual](https://weechat.org/files/doc/stable/weechat_user.en.htm)
- [Get Started on IRC](https://www.irchelp.org/faq/irctutorial.html)
- [Here is a list of useful keyboard
    bindings for weechat](https://weechat.org/files/doc/devel/weechat_quickstart.en.html#key_bindings)
- [A nice guide and introduction to weechat](https://guides.fixato.org/weechat/)
- [Good weechat config to steal stuff from
    😏](https://gist.github.com/pascalpoitras/8406501)
- [IRC CheatSheet](https://gist.github.com/xero/2d6e4b061b4ecbeb9f99)
