On ubuntu 18.04, package available as `libnginx-mod-rtmp`

To consume as rtmp and retransmit
```
rtmp {
	server {
	listen 1935;
	chunk_size 4096;

	application live {
		live on;
		record off;
		interleave off;
		wait_key on;
		meta on;
		wait_video off;
		idle_streams off;
		sync 300ms;
		session_relay on;
		#allow publish 127.0.0.1;
		#allow publish 192.168.2.0/24;
		allow publish all;
		#deny publish all;
		allow play all; 

        # Multistream to these endpoints
        # push rtmp://live-ams.twitch.tv/app/LIVESTREAM_KEY;
        # push rtmp://a.rtmp.youtube.com/live2/LIVESTREAM_KEY;
        # push rtmp://ingest-ams.mixer.com:1935/beam/LIVESTREAM_KEY;
		}
	}
}
```

To playback as dash/hls

```
rtmp {
    server {
        listen 1935; # Listen on standard RTMP port
        chunk_size 4096;

        application show {
            live on;
            # Turn on HLS
            hls on;
            hls_path /tmp/hls/;
            hls_fragment 3;
            hls_playlist_length 60;
            # disable consuming the stream from nginx as rtmp
            deny play all;
        }
    }
}
```

Dash playback

```
server {
    listen 8080;

    location /hls {
        # Disable cache
        add_header Cache-Control no-cache;

        # CORS setup
        add_header 'Access-Control-Allow-Origin' '*' always;
        add_header 'Access-Control-Expose-Headers' 'Content-Length';

        # allow CORS preflight requests
        if ($request_method = 'OPTIONS') {
            add_header 'Access-Control-Allow-Origin' '*';
            add_header 'Access-Control-Max-Age' 1728000;
            add_header 'Content-Type' 'text/plain charset=UTF-8';
            add_header 'Content-Length' 0;
            return 204;
        }

        types {
            application/vnd.apple.mpegurl m3u8;
            video/mp2t ts;
        }

        root /mnt/;
    }
}
```

Expose rtmp stats on an endpoint

```
location /stat {
    rtmp_stat all;
    rtmp_stat_stylesheet stat.xsl;
}
location /stat.xsl {
    root /var/www/html/;
}
#location /control {
# you'll need a htpasswd auth file, that's outside the scope of this doc but any apache one will work
# auth_basic "stream";
# rtmp_control all;
#}
```
