# Hardening nginx

[nginxconfig.io](https://nginxconfig.io/) has a great tool to generate secure
configs.

The basic ones are:
```nginx
# turn server tokens off
server_tokens off;

# generating custom dh params
# sudo openssl dhparam -out /etc/nginx/dhparam.pem 2048
ssl_dhparam          /etc/nginx/dhparam.pem;

# hide headers that can leak info
proxy_hide_header X-Powered-By;
proxy_hide_header X-Generator;

# Secure SSSL ciphers and protocols
ssl_protocols TLSv1.2 TLSv1.3;
ssl_ciphers "EECDH+ECDSA+AESGCM EECDH+aRSA+AESGCM EECDH+ECDSA+SHA384 EECDH+ECDSA+SHA256 EECDH+aRSA+SHA384 EECDH+aRSA+SHA256 EECDH+aRSA+RC4 EECDH EDH+aRSA HIGH !RC4 !aNULL !eNULL !LOW !3DES !MD5 !EXP !PSK !SRP !DSS";
ssl_prefer_server_ciphers on;
ssl_session_cache shared:SSL:50m;
ssl_session_timeout 5m;
```
# Reverse proxy common settings

```nginx
#/etc/nginx/snippets/rp.conf
proxy_http_version                 1.1;
proxy_cache_bypass                 $http_upgrade;

# Proxy headers
proxy_set_header Upgrade           $http_upgrade;
proxy_set_header Connection        "upgrade";
proxy_set_header Host              $host;
proxy_set_header X-Real-IP         $remote_addr;
proxy_set_header X-Forwarded-For   $proxy_add_x_forwarded_for;
proxy_set_header X-Forwarded-Proto $scheme;
proxy_set_header X-Forwarded-Host  $host;
proxy_set_header X-Forwarded-Port  $server_port;

# Proxy timeouts
proxy_connect_timeout              60s;
proxy_send_timeout                 60s;
proxy_read_timeout                 60s;
```

# TCP forwarding

```nginx
stream {
    server {
             listen 2201;
             proxy_pass 192.168.1.99:22;
    }
}
```

or

```nginx
stream {
    upstream myapp {
        server 127.0.0.1:8000 weight=3;
        server 192.168.1.2:80;
        server 192.168.1.3:80;
    }

    server {
             listen 8080;
             proxy_pass myapp;
    }
}
```

More info at
[ngx_http_upstream_module](https://nginx.org/en/docs/http/ngx_http_upstream_module.html)
docs
